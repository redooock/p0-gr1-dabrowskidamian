package pl.edu.uwm.wmii.dabrowskidamian.laboratorium01;
import java.util.Scanner;

public class Zadanie01a2 {

    public static void main(int n) {
        Scanner in = new Scanner(System.in);
        float k = 0;
        for(int i=0;i<n;i++){
            float a = in.nextFloat();
            if(i==1){
                k = a;
            }
            if(i!=1){
                System.out.println(a);
            }
        }
        System.out.println(k);
    }
}

package pl.edu.uwm.wmii.dabrowskidamian.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println(
                "Podziwu godna liczba Pi \n" +
                        "trzy koma jeden cztery jeden. \n" +
                        "Wszystkie jej dalsze cyfry też są początkowe, \n" +
                        "pięć dziewięć dwa ponieważ nigdy się nie kończy. \n" +
                        "Nie pozwala się objąć sześć pięć trzy pięć spojrzeniem \n" +
                        "osiem dziewięć obliczeniem \n" +
                        "siedem dziewięć wyobraźnią, \n" +
                        "a nawet trzy dwa trzy osiem żartem, czyli porównaniem \n" +
                        "cztery sześć do czegokolwiek \n" +
                        "dwa sześć cztery trzy na świecie. \n" +
                        "Najdłuższy ziemski wąż po kilkunastu metrach się urywa \n" +
                        "podobnie, choć trochę później, czynią węże bajeczne. \n" +
                        "Korowód cyfr składających się na liczbę Pi \n" +
                        "nie zatrzymuje się na brzegu kartki, \n" +
                        "potrafi ciągnąc się po stole, przez powietrze, \n" +
                        "przez mur, liść, gniazdo ptasie, chmury, prosto w niebo, \n" +
                        "przez całą nieba wzdętość i bezdenność. \n" +
                        "O, jak krótki, wprost mysi, jest warkocz komety! \n" +
                        "Jak wątły promień gwiazdy, że zakrzywia się w lada przestrzeni! \n" +
                        "A tu dwa trzy piętnaście trzysta dziewiętnaście \n" +
                        "mój numer telefonu twój numer koszuli \n" +
                        "rok tysiąc dziewięćset siedemdziesiąty trzeci szóste piętro \n" +
                        "ilość mieszkańców sześćdziesiąt pięć groszy \n" +
                        "obwód w biodrach dwa palce szarada i szyfr, \n" +
                        "w którym słowiczku mój a leć, a piej \n" +
                        "oraz uprasza się zachować spokój, \n" +
                        "a także ziemia i niebo przeminą, \n" +
                        "ale nie liczba Pi, co to to nie, \n" +
                        "ona wciąż swoje niezłe jeszcze pięć, \n" +
                        "nie byle jakie osiem, \n" +
                        "nieostatnie siedem, \n" +
                        "przynaglając, ach, przynaglając gnuśną wieczność \n" +
                        "do trwania.");


    }
}

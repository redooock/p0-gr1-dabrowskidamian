package pl.edu.uwm.wmii.dabrowskidamian.laboratorium05;

public class Student extends Osoba {

    Student(String nazwisko, int rokUrodzenia, String kierunek){
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    private String kierunek;

    public void getkierunek(){
        System.out.println("Kierunek: " + kierunek);
    }


}

package pl.edu.uwm.wmii.dabrowskidamian.laboratorium05;

import java.util.Optional;
import java.util.OptionalInt;

public class Adres {

    public Adres(String a, int b, int c, String d, String e){
        ulica = a;
        numer_domu = b;
        numer_mieszkania = c;
        miasto = d;
        kod_pocztowy = e;
    }
    public Adres(){
        ulica = "Mieszka";
        numer_domu = 15;
        miasto = "Olsztyn";
        kod_pocztowy = "10-437";
    }
    public Adres(String a, int b, String d, String e){
        ulica = a;
        numer_domu = b;
        miasto = d;
        kod_pocztowy = e;
    }

    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;


    public void pokaz(){
        System.out.println(kod_pocztowy + " " + miasto);
        System.out.println(ulica + " " + numer_domu + "/" + numer_mieszkania);
    }



}

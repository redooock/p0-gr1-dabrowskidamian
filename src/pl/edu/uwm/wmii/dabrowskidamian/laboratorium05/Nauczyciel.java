package pl.edu.uwm.wmii.dabrowskidamian.laboratorium05;

public class Nauczyciel extends Osoba {

    Nauczyciel(String nazwisko, int rokUrodzenia, double pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }
    private double pensja;

    public void getpensja(){
        System.out.println("Pensja: " + pensja);
    }
}

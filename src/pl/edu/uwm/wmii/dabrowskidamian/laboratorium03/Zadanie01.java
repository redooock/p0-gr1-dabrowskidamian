package pl.edu.uwm.wmii.dabrowskidamian.laboratorium03;

import java.util.Scanner;

public class Zadanie01 {

   public static int countChar(String str, char c){
        int l = 0;
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == c){
                l++;
            }

        }
        return l;
    }


    public static int countSubStr(String str, String match){
        int l = 0;


        for(int i = 0; i<=str.length()-match.length();i++){
            if(str.substring(i,i+match.length()).equals(match.substring(0,match.length()))){
                l++;

            }
        }

        return l;

    }


    public static String middle(String str){
       if(str.length()%2==0){
           return str.substring(str.length()/2,(str.length()/2)+2);
       }
       else{
           return str.substring(str.length()/2,str.length()/2+1);
       }
    }


    public static String repeat(String str, int n){
       String wy="";

       for(int i = 0; i <n; i++){
           wy+=str;
       }
       return wy;
    }




    public static void main(String[] args) {

       System.out.println(repeat("ho",3));


    }
}

package pl.edu.uwm.wmii.dabrowskidamian.laboratorium06;

import java.time.LocalDate;
import java.util.*;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalska", "Anna", "Zofia", 1950, 10, 17, 2012, 7, 1, false, 4200.25);
        ludzie[1] = new Student("Nowak", "Piotr", 1994, 12, 22, true, "Informatyka", 4.8);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}


abstract class Osoba
{
    public Osoba(String nazwisko, String imię, String drugieImię, int rokUr, int miesiacUr, int dzienUr, boolean płeć)
    {
        String[] imiona = {imię, drugieImię};

        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = LocalDate.of(rokUr,miesiacUr,dzienUr);
        this.płeć = płeć;
    }

    public Osoba(String nazwisko,String imię, int rokUr, int miesiacUr, int dzienUr, boolean płeć)
    {
        String[] imiona = {imię};

        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = LocalDate.of(rokUr,miesiacUr,dzienUr);
        this.płeć = płeć;
    }


    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String[] imiona(){ return imiona;}

    public LocalDate getdataUrodzenia(){return dataUrodzenia;}

    public boolean getpłeć(){ return płeć;}

    private String nazwisko;

    public String[] imiona;

    public LocalDate dataUrodzenia;

    public boolean płeć;
}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String imię, String drugieImię, int rokUr, int miesiacUr, int dzienUr, int rokZat, int miesiacZat, int dzienZat, boolean płeć, double pobory)
    {
        super( nazwisko, imię, drugieImię, rokUr, miesiacUr, dzienUr, płeć);
        this.pobory = pobory;

        this.dataZatrudnienia = LocalDate.of(rokZat, miesiacZat, dzienZat);
    }
    public Pracownik(String nazwisko, String imię, int rokUr, int miesiacUr, int dzienUr, int rokZat, int miesiacZat, int dzienZat, boolean płeć, double pobory)
    {
        super( nazwisko, imię, rokUr, miesiacUr, dzienUr, płeć);
        this.pobory = pobory;

        this.dataZatrudnienia = LocalDate.of(rokZat, miesiacZat, dzienZat);
    }

    public LocalDate dataZatrudnienia;

    public LocalDate getdataZatrudnienia(){return dataZatrudnienia;}

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    private double pobory;
}


class Student extends Osoba
{
    public Student(String nazwisko, String imię, String drugieImię, int rokUr, int miesiacUr, int dzienUr, boolean płeć, String kierunek, double średniaOcen)
    {
        super(nazwisko, imię, drugieImię, rokUr, miesiacUr, dzienUr, płeć);
        this.kierunek = kierunek;
        this.średniaOcen = średniaOcen;

    }

    public Student(String nazwisko, String imię, int rokUr, int miesiacUr, int dzienUr, boolean płeć, String kierunek, double średniaOcen)
    {
        super(nazwisko, imię, rokUr, miesiacUr, dzienUr, płeć);
        this.kierunek = kierunek;
        this.średniaOcen = średniaOcen;

    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }

    private String kierunek;

    public LocalDate dataZatrudnienia;

    public double średniaOcen;
}

